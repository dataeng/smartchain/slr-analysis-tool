# SLR analysis tool

A simple command-line tool for analyzing historical sensor and weather data.
Visualizes a scatter plot and a linear regression model between two given fields.

## Usage

```
python3 main.py [data_dir] [field_x] [field_y]
```

## Parameters

* `[data_dir]`: 
    A directory that contains JSON files obtained from the SC-History API.
    For example, check the two directories of the project.
    All measurements that appear in all files wil be considered for the analysis.

* `[field_x]`, `[field_y]` :
    Which fields from the measurements to consider for the analysis.
    Could be, for example, `outside_temperature` and `temperature`.
    Regarding the scatter plot visualization, `x` is the horizontal
    axis, while `y` is the vertical axis.
    Regarding the linear regression model, `x` is the feature variable,
    while `y` is the predicted variable.

## Examples

```
python3 main.py ./synthetic_data outside_temperature temperature
python3 main.py ./synthetic_data outside_temperature outside_feels_like
python3 main.py ./synthetic_data timestamp batteryLevel
 ```
 