import matplotlib.pyplot as plt
import json
from scipy import stats
import sys, os

if (len(sys.argv) < 4):
  print("Usage: python3", sys.argv[0], "[data_dir] [field_x] [field_y]")
  exit()

path    = sys.argv[1]
field_x = sys.argv[2]
field_y = sys.argv[3]


files = os.listdir(path)

data = []
for fp in files:
  f = open(path + "/" + fp, "r")
  data = data + json.load(f)
  f.close

x = list(map(lambda i: i[field_x], data))
y = list(map(lambda i: i[field_y], data))

slope, intercept, r, p, std_err = stats.linregress(x, y)

def myfunc(x):
  return slope * x + intercept

mymodel = list(map(myfunc, x))

print("SLOPE     = ", slope)
print("INTERCEPT = ", intercept)
print("R         = ", r)
print("P         = ", p)
print("STD_ERR   = ", std_err)

plt.scatter(x, y, label='original data')
plt.plot(x, mymodel, 'r', label='fitted line')
plt.legend()

plt.xlabel(field_x) 
plt.ylabel(field_y) 

plt.show()
